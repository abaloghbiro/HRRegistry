package hu.braininghub.bh06.ooptraining.hrregistry;

public class Employee extends BusinessObject {

	private String firstName;
	private String lastName;
	private String birthDate;
	private String identityCardNumber;
	private Job currentJob;
	private Integer salary;
	private Employee manager;
	private BusinessObjectFifoQueue jobHistoryQueue = new BusinessObjectFifoQueue();

	public Employee(String id, String birthDate) {
		super(id);
		this.birthDate = birthDate;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdentityCardNumber() {
		return identityCardNumber;
	}

	public void setIdentityCardNumber(String identityCardNumber) {
		this.identityCardNumber = identityCardNumber;
	}

	public Job getCurrentJob() {
		return currentJob;
	}

	public void setCurrentJob(Job currentJob) {
		this.currentJob = currentJob;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public void addJobHistory(JobHistory history) {
		boolean ret = jobHistoryQueue.enqueue(history);
		if (!ret) {
			jobHistoryQueue.dequeue();
			jobHistoryQueue.enqueue(history);
		}
	}

}
