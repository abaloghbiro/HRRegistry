package hu.braininghub.bh06.ooptraining.hrregistry;

public class BusinessObjectFifoQueue {

	public static final int DEFAULT_SIZE = 100;

	private BusinessObject data[];

	private int index;

	public BusinessObjectFifoQueue() {
		data = new BusinessObject[DEFAULT_SIZE];
	}

	public boolean isEmpty() {
		return index == 0;
	}

	public boolean enqueue(BusinessObject obj) {

		if (index == DEFAULT_SIZE - 1) {
			System.out.println("Queue is full. Dequeue some objects");
			return false;
		}
		this.data[index] = obj;
		this.index++;

		System.out.println("Object stored in the queue! { "+obj+" }");

		return true;
	}

	public BusinessObject dequeue() {

		if (isEmpty()) {
			System.out.println("Queue empty please add an element!");
			return null;
		}

		BusinessObject obj = this.data[0];
		for (int i = 0; i < this.index - 1; i++) {
			data[i] = data[i + 1];
		}
		this.index--;
		return obj;
	}
}